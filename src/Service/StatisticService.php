<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\StatisticRepository;


class StatisticService
{
    public function __construct(
        private EntityManagerInterface $manager,
        private StatisticRepository $repositoryStatistic,
    ){}


//----------------------------- Gestion des donnée des graphiques ----------------------------//

    public function getAllStat(): array
    {
        // Récupère la liste de toutes les statistiques
        $statistics = $this->repositoryStatistic->findAll();

        return $statistics;
    }

    public function getMonthlyTotals()
    {
        // Récupère la liste de toutes les statistiques
        $statistics = $this->getAllStat();

        $monthlyTotals = array_fill(0, 12, 0); // Crée un tableau numérique avec 12 éléments initialisés à 0

        // Boucle sur les statistiques
        foreach ($statistics as $statistic) {
            // Récupère le mois de la statistique
            $month = (int) $statistic->getCreatedAt()->format('m');

            // Ajoute le nombre quotidien de la statistique au total mensuel correspondant
            $monthlyTotals[$month - 1] += $statistic->getDailyTax();
        }

        return $monthlyTotals;
    }

    function getStatsForCurrentWeek()
    {
        // Récupérer toutes les statistiques en base de données
        $statistics = $this->getAllStat();

        // Créer un objet DateTimeImmutable pour la date actuelle
        $now = new \DateTimeImmutable();

        // Calculer la date du lundi de cette semaine
        $monday = $now->modify('monday this week')->format('Y-m-d');

        // Calculer la date du jeudi de cette semaine
        $thursday = $now->modify('thursday this week')->format('Y-m-d');

        // Initialiser un tableau de statistiques pour les jours de la semaine actuelle
        $stats = array(0, 0, 0, 0);

        // Parcourir les statistiques et ajouter les valeurs pour les jours de la semaine actuelle
        foreach ($statistics as $stat) {
            $date = $stat->getCreatedAt();
            if ($date >= new \DateTimeImmutable($monday) && $date <= new \DateTimeImmutable($thursday)) {
                $dayOfWeek = $date->format('N') - 1;
                $stats[$dayOfWeek] = $stat->getDailyTax();
            }
        }

        // Retourner les valeurs de statistiques pour les jours de la semaine actuelle
        return $stats;
    }

//--------------------------------------------------------------------------------------------//

}
