<?php

namespace App\Service;

use App\Entity\Activity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Twig\Environment;

/**
 * Classe de gestion des formulaire d'activités.
 * @author Wood Brian
 * @method Activity|null getActivityById(int $id) Retourne une activité en utilisant son identifiant.
 * @method JsonResponse handleAddActivityForm(FormInterface $form) Traiter le formulaire d'ajoute d'activité
 * @method JsonResponse handleUpdateActivityForm(FormInterface $form) Traiter le formulaire de modification d'une activité
 */
class ActivityService extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $manager,
        private Environment $environment,
    ){}

    /**
     * Récupère une activité en utilisant son identifiant.
     *
     * @param int $id   L'identifiant de l'activité recherchée.
     * @return Activity|null    L'objet Activity correspondant à l'identifiant ou null si non trouvé.
     */
    public function getActivityById(int $id): ?Activity
    {
        return $this->manager->getRepository(Activity::class)->find($id);
    }

    //----------------------------------- Ajoute d'une activité ----------------------------------//
    /**
     * Traiter le formulaire d'ajoute d'activité
     *
     * @param FormInterface $addActivityForm Le formulaire envoyer
     * @return JsonResponse Retourner une réponse JSON
     */
    public function handleAddActivityForm(FormInterface $addActivityForm): JsonResponse
    {
        // Vérifier si le formulaire est valide
        if($addActivityForm->isValid()){
            // Si oui, exécuter la fonction pour traiter le formulaire valide
            return $this->handleAddValidForm($addActivityForm);

        }else{
            // Si non, exécuter la fonction pour traiter le formulaire invalide
            return $this->handleAddInvalidForm($addActivityForm);

        }
    }

    /**
     * Traiter le formulaire d'ajoute d'activité valide
     *
     * @param FormInterface $addActivityForm Formulaire valide
     * @return JsonResponse Retourner une réponse JSON
     */
    public function handleAddValidForm(FormInterface $addActivityForm): JsonResponse
    {
        /** @var Activity $activity */
        // On récupère les données soumises dans le formulaire
        $activity = $addActivityForm->getData();

        // On persiste l'activité dans la base de données
        $this->manager->persist($activity);
        $this->manager->flush();

        // On renvoie une réponse JSON avec le code de succès et le code HTML pour afficher l'activité nouvellement créée
        return new JsonResponse([
            'code' => Activity::ACTIVITY_ADDED_SUCCESSFULLY,
            'html' => $this->environment->render('pages/management/_activityElementTab.html.twig', [
                'activity' => $activity,
            ])
        ]);
    }

    /**
     * Traiter le formulaire d'ajoute d'activité invalide
     *
     * @param FormInterface $addActivityForm Formulaire invalide
     * @return JsonResponse Retourner une réponse JSON
     */
    public function handleAddInvalidForm(FormInterface $addActivityForm): JsonResponse
    {
        // Retourner une réponse JSON avec un code d'erreur et les messages d'erreur
        return new JsonResponse([
            'code' => Activity::ACTIVITY_INVALID_FORM,
            'errors' =>  $this->getErrorsMessagesAdd($addActivityForm),
        ]);
    }

    /**
     * Traiter les erreurs du formulaire d'ajoute d'activité
     *
     * @param FormInterface $form Formulaire invalide.
     * @return array Retourner un tableaux d'erreur.
     */
    private function getErrorsMessagesAdd( FormInterface $form): array
    {
        $errors = [];

        // Récupérer les erreurs du formulaire principal
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        // Récupérer les erreurs de tous les champs du formulaire
        foreach ($form->all() as $child) {
            if(!$child->isValid()){
                $errors[$child->getName()] = $this->getErrorsMessagesAdd($child);
            }
        }

        return $errors;
    }
//--------------------------------------------------------------------------------------------//

//----------------------------------- Modifier une activité ----------------------------------//

    public function handleUpdateActivityForm(FormInterface $updateActivityForm): JsonResponse
    {
        // Vérifier si le formulaire est valide
        if ($updateActivityForm->isValid()) {
            // Si oui, exécuter la fonction pour traiter le formulaire valide
            return $this->handleUpdateValidForm($updateActivityForm);
        } else {
            // Si non, exécuter la fonction pour traiter le formulaire invalide
            return $this->handleUpdateInvalidForm($updateActivityForm);
        }
    }

    public function handleUpdateValidForm(FormInterface $updateActivityForm): JsonResponse
    {
        /** @var Activity $activity */
        // Récupérer les données de l'activité actuelle depuis le formulaire
        $currentDataActivity = $updateActivityForm->getData();
        // Récupérer l'identifiant de l'activité à partir des données
        $id = $currentDataActivity->getId();

        // Récupérer les données de l'ancienne activité à partir de l'identifiant
        $oldDataActivity = $this->getActivityById($id);

        // Mettre à jour les champs de l'ancienne activité avec les nouvelles données
        $oldDataActivity
            ->setType($currentDataActivity->getType())
            ->setPlace($currentDataActivity->getPlace());

        // Enregistrer les changements dans la base de données
        $this->manager->persist($oldDataActivity);
        $this->manager->flush();

        // Retourner une réponse JSON avec les données de l'activité mise à jour
        return new JsonResponse([
            'code'  => Activity::ACTIVITY_UPDATE_SUCCESSFULLY,
            'id'    => $id,
            'type'  => $currentDataActivity->getType(),
            'place' => $currentDataActivity->getPlace(),
        ]);
    }

    public function handleUpdateInvalidForm(FormInterface $updateActivityForm): JsonResponse
    {
        // Retourner une réponse JSON avec un code d'erreur et les messages d'erreur
        return new JsonResponse([
            'code' => Activity::ACTIVITY_INVALID_UPDATE_FORM,
            'errors' =>  $this->getErrorsMessagesUpdate($updateActivityForm),
        ]);
    }

    private function getErrorsMessagesUpdate(FormInterface $form): array
    {
        $errors = [];

        // Récupérer les erreurs du formulaire principal
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        // Récupérer les erreurs de tous les champs du formulaire
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorsMessagesUpdate($child);
            }
        }

        return $errors;
    }


//--------------------------------------------------------------------------------------------//

}
