<?php

namespace App\Tools;

use Symfony\UX\Chartjs\Model\Chart;


class Tools
{
    /**
     * Création d'un graphique avec chart de type Line
     *
     * @param ChartBuilderInterface $chartBuilder
     * @param array $labels tableau des label du bas
     * @param string $title texte de la légende
     * @param string $color couleur du graphique
     * @param array $data tableau de donnée
     * @param integer $minValue valeur min d'affichage
     * @param integer $maxValue valeur max d'affichage
     * @return Chart
     */
    public function createChart(
        object $chartBuilder,
        array $labels, 
        string $title, 
        string $color, 
        array $data, 
        int $minValue, 
        int $maxValue
    ): Chart
    {

        $chart = $chartBuilder->createChart(Chart::TYPE_BAR);

        $chart
        ->setData([
            'labels' => $labels,
            'datasets' => [
                [
                    'label' => $title,
                    'backgroundColor' => $color,
                    'borderColor' => $color,
                    'data' => $data,
                ],
            ]
        ])
        ->setOptions([
            'scales' => [
                'y' => [
                    'suggestedMin' => $minValue,
                    'suggestedMax' => $maxValue,
                ],
            ],
        ]);
        
        return $chart;
    }

    public function createChartPie(
        object $chartBuilder,
        array $labels,
        string $title,
        array $color,
        array $data,
    ): Chart {

        $chart = $chartBuilder->createChart(Chart::TYPE_DOUGHNUT);

        $chart
            ->setData([
                'labels' => $labels,
                'datasets' => [
                    [
                        'label' => $title,
                        'backgroundColor' => $color,
                        'borderColor' => "#FFF",
                        'data' => $data,
                    ],
                ]
                ]);

        return $chart;
    }
    
}
