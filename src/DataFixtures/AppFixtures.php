<?php

namespace App\DataFixtures;

use App\Entity\Intern;
use App\Entity\Room;
use App\Entity\Emergency;
use App\Entity\Wifi;
use App\Entity\Subscribe;
use App\Entity\Inventory;
use App\Entity\Activity;
use App\Entity\Statistic;
use App\Entity\User;
use App\Entity\Employee;
use App\Entity\Observation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

// Création des jeux de donnée pour la BDD
class AppFixtures extends Fixture 
{
    /**
     *
     * @var Generator
     */
    private Generator $faker;


    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }
    
    public function load(ObjectManager $manager): void
    {

        $liste = array(); // initialiser la liste

        for ($i = 1; $i <= 40; $i++) {
            $index = $this->faker->numberBetween(1, 20) . chr(rand(65, 66)); // générer un index aléatoire
            while (in_array($index, $liste)) { // vérifier si l'index est déjà présent dans la liste
                $index = $this->faker->numberBetween(1, 20) . chr(rand(65, 66)); // générer un nouvel index jusqu'à ce qu'il soit unique
            }
            $liste[$i] = $index; // ajouter l'index à la liste
        }

        $listeActivity = array();

        // Génération des activités
        $listActivity = ['Piscine', 'Ping-Pong', 'Marche', 'Jeux de sociéter', 'Sport'];
        $listPlaceActivity = ['St-Paul','AFPAR','Savane','AFPAR','St-Paul'];
        for ($idActivity=0; $idActivity < 5; $idActivity++) {
            $activity = new Activity();
            
            $activity
                ->setType($listActivity[$idActivity])
                ->setPositioning($this->faker->numberBetween(0, 20))
                ->setPlace($listPlaceActivity[$idActivity]);

            $manager->persist($activity);

            $listeActivity[$idActivity] = $activity;
        }
        

        for ($idIntern=1; $idIntern <= 20; $idIntern++) {
            $user = new User();
            $intern = new Intern();
            $emergency = new Emergency();
            $wifi = new Wifi();
            $room = new Room();
            $inventory = new Inventory();
            

            $sex = $this->faker->randomElement(['male', 'female','autre']);
            $formationTab = 
                [
                    'GP' => 'Gestionnaire de Paie',
                    'SAM' => 'Secrétaire Assistant(e) Médico-Social ',
                    'SA' => 'Secrétaire Assistant(e) (Blended Learning)',
                    'DE' => 'Digital Explorer',
                    'ADVF' => 'Assistant(e) De Vie aux Familles',
                    'P' => 'Plaquiste',
                    'APEB' => 'Agent(e) Polyvalent(e) d\'Exécution en Bâtiment',
                    'EAA' => 'Employé(e) Administratif(ve) et d\'Accueil',
                    'CCC' => 'Carreleur(se)-Chapiste',
                    'ARH' => 'Assistant(e) Ressources Humaines',
                    'MRCV' => 'Mécanicien(ne) Réparateur(trice) de Cycles et de Vélos à assistance électrique',
                    'CA' => 'Comptable Assistant(e)',
                    'FPA' => 'Formateur(trice) Professionnelle d\'Adultes',
                    'AMB' => 'Agent(e) de Maintenance des Bâtiments',
                    'DWWM' => 'Développeur(se) Web et Web Mobile',
                    'RCSD' => 'Responsable Coordonnateur(trice) Services au Domicile',
                    'AD' => 'Assistant(e) de Direction',
                    'SC' => 'Secrétaire Comptable',
                ];
            
            $sigle = array_rand($formationTab);
            $formation = $formationTab[$sigle];

            // Génération de chambre pour l'interne
            $room
                ->setRoomNumber($liste[$idIntern])
                ->setKeyState($keyState = $this->faker->numberBetween(0, 1))
                ->setInventory(
                    $inventory
                        ->setRoomState($keyState)
                        ->setRoomComment($this->faker->words(3, true))
                        ->setPerformed($this->faker->dateTime())
                        ->setRoom($room)
                );
            
            // Génération de l'interne
            $intern
                ->setLastName($lastName = $this->faker->lastName())
                ->setFirstName($firstName = $this->faker->firstName($gender = $sex))
                ->setPhoneNumberIntern('0692' . $this->faker->regexify('[0-9]{6}'))
                ->setMailIntern(strtolower($lastName) . strtolower($firstName) . '@exemple.fr')
                ->setNumberPlate($this->faker->regexify('[A-Z]{2}[-][0-9]{3}[-][A-Z]{2}'))
                ->setFormation($formation)
                ->setFormationAbbreviation($sigle)
                ->setArrivingIntern($startArrivingDate = $this->faker->dateTime())
                ->setFinishingIntern((clone $startArrivingDate)->modify('+' . $this->faker->numberBetween(6, 12) . ' month'))
                ->setToNight($this->faker->randomNumber(2, false))
                ->setRemainingNight($this->faker->randomNumber(2, false))
                ->setImageProfil($this->faker->imageUrl(640, 480, 'animals', true))
                ->setSerialNumIntern($this->faker->regexify('[A-Z]{5}[0-4]{3}'))
                ->setBirthDate($this->faker->dateTime())
                ->setAddressIntern($this->faker->address())
                ->setCity($this->faker->city())
                ->setRoom($room)
                ->setEmergency(
                    $emergency
                        ->setLastName($this->faker->lastName())
                        ->setFirstName($this->faker->firstName($gender = 'male' | 'female'))
                        ->setPhoneNumber('0692' . $this->faker->regexify('[0-9]{6}'))
                )
                ->setWifi(
                    $wifi
                        ->setWifiPassword($this->faker->regexify('[A-Z]{5}[0-9]{3}'))
                )

                ->setZipCode(974 . $this->faker->regexify('[0-9]{2}'));
                if($sex == 'male'){
                    $intern->setSexIntern('H');
                }else if($sex == 'female'){
                    $intern->setSexIntern('F');
                }else{
                    $intern->setSexIntern('X');
                };
            
            // Génération d'un nombre aléatoire d'inscription a des activités pour l'interne
            $randomNumberSubscribe = $this->faker->numberBetween(0, 4);
                            
            for ($idSub = 0; $idSub <= $randomNumberSubscribe; $idSub++) {
                
                $subscribe = new Subscribe();
                
                $subscribe
                ->setIntern($intern)
                ->setActivity($listeActivity[$idSub]);
                
                $manager->persist($subscribe);

                $intern->addSubscribe($subscribe);
            }

            $manager->persist($intern);

            // Génération d'un nombre aléatoire d'observation pour l'interne
            $randomNumberObservation = $this->faker->numberBetween(0, 2);
            for ($idObs = 0; $idObs <= $randomNumberObservation; $idObs++) {
                
                $observation = new Observation();

                $type = $this->faker->randomElement(['absence', 'signalement']);

                $observation
                    ->setIntern($intern)
                    ->setType($type);
                    if ($type == 'absence') {
                        $observation
                            ->setStartDate($startDate = $this->faker->dateTime())
                            ->setEndDate((clone $startDate)->modify('+3 days'))
                            ->setDescription(null)
                            ->setImages([])
                            ->setObservationState($this->faker->randomElement(['Envoyé', 'En cours', 'traité']));
                    } else {
                        $observation
                            ->setStartDate(null)
                            ->setEndDate(null)
                            ->setDescription($this->faker->words(5, true))
                            ->setObservationState($this->faker->randomElement(['Envoyé', 'En cours', 'traité']));
                        
                        $images = array();

                        for ($image=0; $image < $this->faker->numberBetween(0, 3); $image++) { 
                            $images[] = $this->faker->imageUrl(640, 480, 'animals', true);
                        }
                        $observation
                            ->setImages($images);
                    }

                $manager->persist($observation);
                
            }

            // Génération d'un user pour l'interne
            $user
                ->setIntern($intern)
                ->setFullName($intern->getLastName()." ".$intern->getFirstName())
                ->setPlainePassword('password')
                ->setEmployee(null)
                ->setRoles(['ROLE_USER'])
                ->setLogin($intern->getMailIntern());

            $manager->persist($user);

            
        };

        // Génération de 20 places de chambres supplémentaires
        for ($id=21; $id <= 40; $id++) {
            $room2 = new Room();
            $inventory2 = new Inventory();
            $room2
                ->setRoomNumber($liste[$id])
                ->setKeyState($keyState = $this->faker->numberBetween(0, 1))
                ->setInventory(
                $inventory2
                        ->setRoomState($keyState)
                        ->setRoomComment($this->faker->words(3, true))
                        ->setPerformed($this->faker->dateTime())
                        ->setRoom($room2)
                );
                $manager->persist($room2);
        }

        // Génération de 5 employée et user
        for ($i = 1; $i <= 5; $i++) {
            $userManager = new User();
            $employee = new Employee();

            $employee
                ->setLastName($lastName = $this->faker->lastName())
                ->setFirstName($firstName = $this->faker->firstName($gender = ['male', 'female']))
                ->setEmail(strtolower($lastName) . strtolower($firstName) . '@exemple.fr');
            $manager->persist($employee);

            $userManager
                ->setEmployee($employee)
                ->setIntern(null)
                ->setFullName($employee->getLastName()." ".$employee->getFirstName())
                ->setPlainePassword('password')
                ->setRoles(['ROLE_ADMIN'])
                ->setLogin($employee->getEmail());

            $manager->persist($userManager);
        };

        // Génération de statistiques depuis le début de l'année jusqu'à aujourd'hui

        $currentYear = date('Y');                                       // Récupère l'année actuelle
        $startDate = new \DateTimeImmutable('01-01-' . $currentYear);   // Crée la date de début de l'année actuelle
        $endDate = new \DateTimeImmutable(date('d-m-Y'));               // Crée la date d'aujourd'hui
        $interval = new \DateInterval('P1D');                           // Crée un intervalle de 1 jour
        $dateRange = new \DatePeriod($startDate, $interval, $endDate);  // Crée une période de dates entre la date de début et la date de fin

        foreach ($dateRange as $date) {
            $statistic = new Statistic();                               // Crée une nouvelle instance de la classe Statistic
            $statistic
            ->setDailyTax($this->faker->numberBetween(0, 20))           // Génère un nombre aléatoire entre 0 et 20 pour la taxe quotidienne
            ->setCreatedAt($date);                                      // Définit la date de création de la statistique
            $manager->persist($statistic);                              // Ajoute l'objet Statistic à la liste des objets à persister en base de données
        }

    // --------------------------------- Génération de L'interne et employer de test --------------------------- //

        $userTest = new User();
        $internTest = new Intern();
        $emergencyTest = new Emergency();
        $wifiTest = new Wifi();
        $roomTest = new Room();
        $inventoryTest = new Inventory();

        // Génération de chambre pour l'interne
        $roomTest
        ->setRoomNumber($liste[21])
        ->setKeyState($keyState = $this->faker->numberBetween(0, 1))
        ->setInventory(
            $inventoryTest
                ->setRoomState($keyState)
                ->setRoomComment($this->faker->words(3, true))
                ->setPerformed($this->faker->dateTime())
                ->setRoom($roomTest)
        );

        // Génération de l'interne
        $internTest
        ->setLastName("Test")
        ->setFirstName("Interne")
        ->setPhoneNumberIntern('0692000000')
        ->setMailIntern('testinterne@exemple.fr')
        ->setNumberPlate('AB-000-CD')
        ->setFormation('Développeur(se) Web et Web Mobile')
        ->setFormationAbbreviation('DWWM')
        ->setArrivingIntern($startArrivingDate = $this->faker->dateTime())
        ->setFinishingIntern((clone $startArrivingDate)->modify('+' . $this->faker->numberBetween(6, 12) . ' month'))
        ->setToNight($this->faker->randomNumber(2, false))
        ->setRemainingNight($this->faker->randomNumber(2, false))
        ->setImageProfil($this->faker->imageUrl(640, 480, 'animals', true))
        ->setSerialNumIntern('DWWM.M01')
        ->setBirthDate($this->faker->dateTime())
        ->setAddressIntern($this->faker->address())
        ->setCity($this->faker->city())
        ->setRoom($roomTest)
        ->setEmergency(
            $emergencyTest
                ->setLastName($this->faker->lastName())
                ->setFirstName($this->faker->firstName($gender = 'male' | 'female'))
                ->setPhoneNumber('0692' . $this->faker->regexify('[0-9]{6}'))
        )
        ->setWifi(
            $wifiTest
                ->setWifiPassword($this->faker->regexify('[A-Z]{5}[0-9]{3}'))
        )
        ->setZipCode(974 . $this->faker->regexify('[0-9]{2}'))
        ->setSexIntern('H');

        for ($idSub = 0; $idSub <= $this->faker->numberBetween(0, 4); $idSub++) {

            $subscribe = new Subscribe();

            $subscribe
                ->setIntern($internTest)
                ->setActivity($listeActivity[$idSub]);

            $manager->persist($subscribe);

            $internTest->addSubscribe($subscribe);
        }

        $manager->persist($internTest);

        // Génération d'un nombre aléatoire d'observation pour l'interne
        for ($idObs = 0; $idObs <= $this->faker->numberBetween(0, 2); $idObs++) {

            $observation = new Observation();

            $type = $this->faker->randomElement(['absence', 'signalement']);

            $observation
                ->setIntern($internTest)
                ->setType($type);
            if ($type == 'absence') {
                $observation
                ->setStartDate($startDate = $this->faker->dateTime())
                ->setEndDate((clone $startDate)->modify('+3 days'))
                ->setDescription(null)
                ->setImages([])
                    ->setObservationState($this->faker->randomElement(['Envoyé', 'En cours', 'traité']));
            } else {
                $observation
                    ->setStartDate(null)
                    ->setEndDate(null)
                    ->setDescription($this->faker->words(5, true))
                    ->setObservationState($this->faker->randomElement(['Envoyé', 'En cours', 'traité']));

                    $images = array();

                    for ($image=0; $image < $this->faker->numberBetween(0, 3); $image++) { 
                        $images[] = $this->faker->imageUrl(640, 480, 'animals', true);
                    }
                    $observation
                        ->setImages($images);
            }

            $manager->persist($observation);
        }

        // Génération d'un user pour l'interne
        $userTest
            ->setIntern($internTest)
            ->setFullName("Test Interne")
            ->setPlainePassword('password')
            ->setEmployee(null)
            ->setRoles(['ROLE_USER'])
            ->setLogin('testinterne@exemple.fr');

        $manager->persist($userTest);

        // Génération d'un employée de test
        $userManagerTest = new User();
        $employeeTest = new Employee();

        $employeeTest
            ->setLastName("Test")
            ->setFirstName("Manager")
            ->setEmail('testmanager@exemple.fr');

        $manager->persist($employeeTest);

        $userManagerTest
            ->setEmployee($employeeTest)
            ->setIntern(null)
            ->setFullName("Test Manager")
            ->setPlainePassword('password')
            ->setRoles(['ROLE_ADMIN'])
            ->setLogin('testmanager@exemple.fr');

        $manager->persist($userManagerTest);

        // Envoie des données en base de données
        $manager->flush();
    }
}
