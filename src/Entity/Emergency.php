<?php

namespace App\Entity;


use App\Repository\EmergencyRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


#[ORM\Entity(repositoryClass: EmergencyRepository::class)]
class Emergency
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 45, nullable: true)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 45)]
    #[Assert\Type('string')]
    private ?string $lastName = null;

    #[ORM\Column(length: 45, nullable: true)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 45)]
    #[Assert\Type('string')]
    private ?string $firstName = null;

    #[ORM\Column(length: 25, nullable: true)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 10, max: 25)]
    #[Assert\Type('string')]
    private ?string $phoneNumber = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }
}
