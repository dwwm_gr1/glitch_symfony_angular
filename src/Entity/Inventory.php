<?php

namespace App\Entity;

use App\Repository\InventoryRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: InventoryRepository::class)]
class Inventory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    #[Assert\NotBlank()]
    #[Assert\Type('int')]
    private ?int $roomState = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Assert\Length(min: 2, max: 100)]
    #[Assert\Type('string')]
    private ?string $roomComment = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $performed = null;

    #[ORM\OneToOne(mappedBy: 'Inventory', cascade: ['persist', 'remove'])]
    private ?Room $room = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoomState(): ?int
    {
        return $this->roomState;
    }

    public function setRoomState(int $roomState): self
    {
        $this->roomState = $roomState;

        return $this;
    }

    public function getRoomComment(): ?string
    {
        return $this->roomComment;
    }

    public function setRoomComment(?string $roomComment): self
    {
        $this->roomComment = $roomComment;

        return $this;
    }

    public function getPerformed(): ?\DateTimeInterface
    {
        return $this->performed;
    }

    public function setPerformed(\DateTimeInterface $performed): self
    {
        $this->performed = $performed;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(Room $room): self
    {
        // set the owning side of the relation if necessary
        if ($room->getInventory() !== $this) {
            $room->setInventory($this);
        }

        $this->room = $room;

        return $this;
    }
}
