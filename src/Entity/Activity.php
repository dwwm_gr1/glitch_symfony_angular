<?php

namespace App\Entity;

use App\Repository\ActivityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * La structure d'une activité.
 * @var int $id L'identifiant.
 * @var string $type Le nom.
 * @var int $positioning Le nombre de participant.
 * @var string $place Le lieu.
 * @var Collection $subscribes Les interne inscrits.
 */
#[ORM\Entity(repositoryClass: ActivityRepository::class)]
class Activity
{
    // Les code d'erreur
    const ACTIVITY_ADDED_SUCCESSFULLY = 'ACTIVITY_ADDED_SUCCESSFULLY';
    const ACTIVITY_UPDATE_SUCCESSFULLY = 'ACTIVITY_UPDATE_SUCCESSFULLY';
    const ACTIVITY_INVALID_UPDATE_FORM = 'ACTIVITY_INVALID_UPDATE_FORM'; 
    const ACTIVITY_INVALID_FORM = 'ACTIVITY_INVALID_FORM'; 

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 45)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 45)]
    private ?string $type = null;

    #[ORM\Column]
    #[Assert\Type('int')]
    #[Assert\PositiveOrZero]
    private ?int $positioning = null;

    #[ORM\Column(length: 45, nullable: true)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 45)]
    private ?string $place = null;

    #[ORM\OneToMany(mappedBy: 'activity', targetEntity: Subscribe::class, orphanRemoval: true)]
    private Collection $subscribes;

    public function __construct()
    {
        $this->subscribes = new ArrayCollection();
        $this->positioning = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPositioning(): ?int
    {
        return $this->positioning;
    }

    public function setPositioning(int $positioning): self
    {
        $this->positioning = $positioning;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(?string $place): self
    {
        $this->place = $place;

        return $this;
    }

    /**
     * @return Collection<int, Subscribe>
     */
    public function getSubscribes(): Collection
    {
        return $this->subscribes;
    }

    public function addSubscribe(Subscribe $subscribe): self
    {
        if (!$this->subscribes->contains($subscribe)) {
            $this->subscribes->add($subscribe);
            $subscribe->setActivity($this);
        }

        return $this;
    }

    public function removeSubscribe(Subscribe $subscribe): self
    {
        if ($this->subscribes->removeElement($subscribe)) {
            // set the owning side to null (unless already changed)
            if ($subscribe->getActivity() === $this) {
                $subscribe->setActivity(null);
            }
        }

        return $this;
    }

}
