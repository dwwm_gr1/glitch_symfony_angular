<?php

namespace App\Entity;

use App\Repository\EmployeeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: EmployeeRepository::class)]
class Employee
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 45)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 45)]
    #[Assert\Type('string')]
    private ?string $lastName = null;

    #[ORM\Column(length: 45)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 45)]
    #[Assert\Type('string')]
    private ?string $firstName = null;

    #[ORM\Column(length: 45)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 45)]
    #[Assert\Type('string')]
    #[Assert\Email(
        message: 'L\'e-mail {{ value }} n\'est pas un e-mail valide.',
    )]
    private ?string $email = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
