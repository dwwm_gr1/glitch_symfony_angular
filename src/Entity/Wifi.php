<?php

namespace App\Entity;

use App\Repository\WifiRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


#[ORM\Entity(repositoryClass: WifiRepository::class)]
class Wifi
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 25)]
    #[Assert\NotNull()]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 8, max: 25)]
    #[Assert\Type('string')]
    private ?string $wifiPassword = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWifiPassword(): ?string
    {
        return $this->wifiPassword;
    }

    public function setWifiPassword(string $wifiPassword): self
    {
        $this->wifiPassword = $wifiPassword;

        return $this;
    }
}
