<?php

namespace App\Entity;

use App\Repository\RoomRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


#[ORM\Entity(repositoryClass: RoomRepository::class)]
class Room
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 5)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 5)]
    #[Assert\Type('string')]
    private ?string $roomNumber = null;

    #[ORM\Column]
    #[Assert\NotBlank()]
    #[Assert\Length(1)]
    #[Assert\Choice([ 0, 1 ])]
    #[Assert\Type('int')]
    private ?int $keyState = null;

    #[ORM\OneToOne(inversedBy: 'room', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Inventory $Inventory = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoomNumber(): ?string
    {
        return $this->roomNumber;
    }

    public function setRoomNumber(string $roomNumber): self
    {
        $this->roomNumber = $roomNumber;

        return $this;
    }

    public function getKeyState(): ?int
    {
        return $this->keyState;
    }

    public function setKeyState(int $keyState): self
    {
        $this->keyState = $keyState;

        return $this;
    }

    public function getInventory(): ?Inventory
    {
        return $this->Inventory;
    }

    public function setInventory(Inventory $Inventory): self
    {
        $this->Inventory = $Inventory;

        return $this;
    }


}
