<?php

namespace App\Entity;

use App\Repository\InternRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: InternRepository::class)]
class Intern
{

    const INTERN_GETCARD_SUCCESSFULLY = 'INTERN_GETCARD_SUCCESSFULLY';
    const INTERN_INVALID_CARD = 'INTERN_INVALID_CARD'; 

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 30)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 30)]
    #[Assert\Type('string')]
    private ?string $lastName = null;

    #[ORM\Column(length: 30)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 30)]
    #[Assert\Type('string')]
    private ?string $firstName = null;

    #[ORM\Column(length: 45)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 45)]
    #[Assert\Type('string')]
    private ?string $serialNumIntern = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Assert\NotNull()]
    #[Assert\Date]
    private ?\DateTimeInterface $birthDate = null;

    #[ORM\Column(length: 50)]
    #[Assert\NotNull()]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 50)]
    #[Assert\Type('string')]
    private ?string $addressIntern = null;

    #[ORM\Column(length: 45)]
    #[Assert\NotNull()]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 45)]
    #[Assert\Type('string')]
    private ?string $city = null;

    #[ORM\Column]
    #[Assert\NotNull()]
    #[Assert\Positive()]
    #[Assert\Type('int')]
    private ?int $zipCode = null;

    #[ORM\Column(length: 1)]
    #[Assert\NotBlank()]
    #[Assert\NotNull()]
    #[Assert\Length(1)]
    #[Assert\Choice(['H', 'F', 'X'])]
    #[Assert\Type('string')]
    private ?string $sexIntern = null;

    #[ORM\Column(length: 25)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 10, max: 25)]
    #[Assert\Type('string')]
    private ?string $phoneNumberIntern = null;

    #[ORM\Column(length: 40)]
    #[Assert\NotBlank()]
    #[Assert\NotNull()]
    #[Assert\Email(
        message: 'L\'email {{ value }} n\'est pas valide.',
    )]
    private ?string $mailIntern = null;

    #[ORM\Column(length: 15, nullable: true)]
    #[Assert\Length(min: 2, max: 15)]
    #[Assert\Type('string')]
    private ?string $numberPlate = null;

    #[ORM\Column(length: 100)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 2, max: 100)]
    #[Assert\Type('string')]
    private ?string $formation = null;

    #[ORM\Column(length: 10)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 1, max: 10)]
    #[Assert\Type('string')]
    private ?string $formationAbbreviation = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Assert\DateTime]
    #[Assert\NotBlank()]
    private ?\DateTimeInterface $arrivingIntern = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Assert\DateTime]
    #[Assert\NotBlank()]
    private ?\DateTimeInterface $finishingIntern = null;

    #[ORM\Column]
    #[Assert\NotBlank()]
    #[Assert\Type('int')]
    private ?int $toNight = null;

    #[ORM\Column]
    #[Assert\NotBlank()]
    #[Assert\Type('int')]
    private ?int $remainingNight = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $imageProfil = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Room $Room = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Emergency $Emergency = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Wifi $Wifi = null;

    #[ORM\OneToMany(mappedBy: 'intern', targetEntity: Observation::class)]
    private Collection $Observations;

    #[ORM\OneToMany(mappedBy: 'intern', targetEntity: Subscribe::class, orphanRemoval: true)]
    private Collection $subscribes;


    public function __construct()
    {
        $this->Observations = new ArrayCollection();
        $this->subscribes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getSerialNumIntern(): ?string
    {
        return $this->serialNumIntern;
    }

    public function setSerialNumIntern(string $serialNumIntern): self
    {
        $this->serialNumIntern = $serialNumIntern;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getAddressIntern(): ?string
    {
        return $this->addressIntern;
    }

    public function setAddressIntern(string $addressIntern): self
    {
        $this->addressIntern = $addressIntern;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getZipCode(): ?int
    {
        return $this->zipCode;
    }

    public function setZipCode(int $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getSexIntern(): ?string
    {
        return $this->sexIntern;
    }

    public function setSexIntern(string $sexIntern): self
    {
        $this->sexIntern = $sexIntern;

        return $this;
    }

    public function getPhoneNumberIntern(): ?string
    {
        return $this->phoneNumberIntern;
    }

    public function setPhoneNumberIntern(string $phoneNumberIntern): self
    {
        $this->phoneNumberIntern = $phoneNumberIntern;

        return $this;
    }

    public function getMailIntern(): ?string
    {
        return $this->mailIntern;
    }

    public function setMailIntern(string $mailIntern): self
    {
        $this->mailIntern = $mailIntern;

        return $this;
    }

    public function getNumberPlate(): ?string
    {
        return $this->numberPlate;
    }

    public function setNumberPlate(?string $numberPlate): self
    {
        $this->numberPlate = $numberPlate;

        return $this;
    }

    public function getFormation(): ?string
    {
        return $this->formation;
    }

    public function setFormation(string $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    public function getFormationAbbreviation(): ?string
    {
        return $this->formationAbbreviation;
    }

    public function setFormationAbbreviation(string $formationAbbreviation): self
    {
        $this->formationAbbreviation = $formationAbbreviation;

        return $this;
    }

    public function getArrivingIntern(): ?\DateTimeInterface
    {
        return $this->arrivingIntern;
    }

    public function setArrivingIntern(\DateTimeInterface $arrivingIntern): self
    {
        $this->arrivingIntern = $arrivingIntern;

        return $this;
    }

    public function getFinishingIntern(): ?\DateTimeInterface
    {
        return $this->finishingIntern;
    }

    public function setFinishingIntern(\DateTimeInterface $finishingIntern): self
    {
        $this->finishingIntern = $finishingIntern;

        return $this;
    }

    public function getToNight(): ?int
    {
        return $this->toNight;
    }

    public function setToNight(int $toNight): self
    {
        $this->toNight = $toNight;

        return $this;
    }

    public function getRemainingNight(): ?int
    {
        return $this->remainingNight;
    }

    public function setRemainingNight(int $remainingNight): self
    {
        $this->remainingNight = $remainingNight;

        return $this;
    }

    public function getImageProfil(): ?string
    {
        return $this->imageProfil;
    }

    public function setImageProfil(?string $imageProfil): self
    {
        $this->imageProfil = $imageProfil;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->Room;
    }

    public function setRoom(Room $Room): self
    {
        $this->Room = $Room;

        return $this;
    }

    public function getEmergency(): ?Emergency
    {
        return $this->Emergency;
    }

    public function setEmergency(Emergency $Emergency): self
    {
        $this->Emergency = $Emergency;

        return $this;
    }

    public function getWifi(): ?Wifi
    {
        return $this->Wifi;
    }

    public function setWifi(Wifi $Wifi): self
    {
        $this->Wifi = $Wifi;

        return $this;
    }

    /**
     * @return Collection<int, Observation>
     */
    public function getObservations(): Collection
    {
        return $this->Observations;
    }

    public function addObservation(Observation $observation): self
    {
        if (!$this->Observations->contains($observation)) {
            $this->Observations->add($observation);
            $observation->setIntern($this);
        }

        return $this;
    }

    public function removeObservation(Observation $observation): self
    {
        if ($this->Observations->removeElement($observation)) {
            // set the owning side to null (unless already changed)
            if ($observation->getIntern() === $this) {
                $observation->setIntern(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Subscribe>
     */
    public function getSubscribes(): Collection
    {
        return $this->subscribes;
    }

    public function addSubscribe(Subscribe $subscribe): self
    {
        if (!$this->subscribes->contains($subscribe)) {
            $this->subscribes->add($subscribe);
            $subscribe->setIntern($this);
        }

        return $this;
    }

    public function removeSubscribe(Subscribe $subscribe): self
    {
        if ($this->subscribes->removeElement($subscribe)) {
            // set the owning side to null (unless already changed)
            if ($subscribe->getIntern() === $this) {
                $subscribe->setIntern(null);
            }
        }

        return $this;
    }

}
