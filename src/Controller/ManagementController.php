<?php

namespace App\Controller;

use App\Entity\Activity;
use App\Form\AddActivityType;
use App\Form\UpdateActivityType;
use App\Tools\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\InternRepository;
use App\Repository\RoomRepository;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use App\Repository\ActivityRepository;
use App\Service\ActivityService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ManagementController extends AbstractController
{

    #[Route('/Management', 'management.index', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function index(
            ChartBuilderInterface $chartBuilder, 
            InternRepository $repositoryIntern,
            RoomRepository $repositoryRoom,
            ActivityRepository $activityRepository,
            Request $request,
            ActivityService $activityService,
            Tools $tools,
        ): Response
    {
    //---------------------------------------- Les Variable --------------------------------------//
        // Récupérer la liste des internes
        $listInterns = $repositoryIntern->findAll();
        // Compter le nombre total d'internes
        $totInterns = count($listInterns);
        // Initialiser le compteur des internes présents
        $internHere = 0;

        // Récupérer la liste des chambres
        $listRoom = $repositoryRoom->findAll();
        // Compter le nombre total de chambres
        $totRoom = count($listRoom);
        // Initialiser le compteur des chambres remplies
        $roomFill = 0;

        // Récupérer la liste des activités
        $listActivity = $activityRepository->findAll();
    //--------------------------------------------------------------------------------------------//
    
    
    //---------------------------------- Gestion des détail de statistic -------------------------//
        // Parcourir la liste des internes pour récupérer le nombre d'internes présents
        foreach ($listInterns as $intern) {
            if($intern->getRoom()->getKeyState() == 0){
                $internHere++;
            }
        }

        // Parcourir la liste des chambres pour récupérer le nombre de chambres remplies
        foreach ($listRoom as $room) {
            if ($room->getKeyState() == 0) {
                $roomFill++;
            }
        }

        // Calculer le taux de remplissage des chambres
        $dailyTax = ($roomFill / $totRoom * 100) . '%' ;
    //--------------------------------------------------------------------------------------------//

        
    //----------------------------------- Création du graphique ----------------------------------//
        // Créer un graphique avec les données des internes présents
        $chart = $tools->createChart(
            $chartBuilder, 
            ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi'],
            'Taux Hebdomadaire',
            '#008C40',
            [12,15,4,10,8],
            0,
            $totInterns
        );
    //--------------------------------------------------------------------------------------------//


    //------------------------------------ Ajoute d'une activité ---------------------------------//
        // Créer un formulaire pour ajouter une nouvelle activité
        $addActivityForm = $this->createForm(AddActivityType::class, $activityRepository->new());
        $addActivityForm->handleRequest($request);

        // Si le formulaire est soumis, traiter la demande
        if($addActivityForm->isSubmitted()){
           return $activityService->handleAddActivityForm($addActivityForm);
        }
    //--------------------------------------------------------------------------------------------//



    //------------------------------------ Modifier d'une activité ---------------------------------//
        // Créer un formulaire pour modifier une activité existante
        $updateActivityForm = $this->createForm(UpdateActivityType::class);
        $updateActivityForm->handleRequest($request);

        // Si le formulaire est soumis, traiter la demande
        if ($updateActivityForm->isSubmitted()) {
            return $activityService->handleUpdateActivityForm($updateActivityForm);
        }
    //--------------------------------------------------------------------------------------------//


    //------------------------------------ Elements retourner à la page --------------------------//
        // Retourner la page avec les données nécessaires pour l'affichage
        return $this->render('pages/management/index.html.twig', [
            'formAddActivity' => $addActivityForm->createView(),
            'formUpdateActivity' => $updateActivityForm->createView(),
            'dailyTax' => $dailyTax,
            'totRoom' => $totRoom,
            'roomFill' => $roomFill,
            'internHere' => $internHere,
            'totInterns' => $totInterns,
            'interns' => $listInterns,
            'chart' => $chart,
            'listActivity' => $listActivity,
        ]);
    //--------------------------------------------------------------------------------------------//

    }

    //------------------------------------ récupérer une activité ------------------------------------//
        // Cette méthode est appelée lorsqu'un utilisateur ouvre la modal de modification d'activité
        #[Route('/Management/activity/get', 'activity_get', methods: ['GET'])]
        #[IsGranted('ROLE_ADMIN')]
        public function getActivity(Request $request, ActivityService $activityService)
        {
            // Récupérer l'identifiant de l'activité depuis la requête
            $id = $request->query->get('id');

            // Récupérer l'activité correspondante depuis le service ActivityService
            $activity = $activityService->getActivityById($id);

            // Retourner une réponse au format JSON contenant les informations de l'activité
            return new JsonResponse([
                'id' => $activity->getId(),
                'type' => $activity->getType(),
                'place' => $activity->getPlace(),
            ]);
        }
    //------------------------------------------------------------------------------------------------//


    //------------------------------------ Affichage de la carte interne -----------------------------//
        // Cette méthode est appelée lorsqu'un utilisateur souhaite voir la carte d'un interne
        #[Route('/Management/InternCard/{id}', 'management.internCard', methods: ['GET'])]
        #[IsGranted('ROLE_ADMIN')]
        public function getCard(
            InternRepository $repositoryIntern,
            int $id,
        ): Response
        {
            // Récupère l'interne correspondant à l'ID
            $intern = $repositoryIntern->findOneBy(["id" => $id]);

            // Renvoie la carte d'affichage de l'interne
            return $this->render('pages/management/_cardIntern.html.twig', [
                'intern' => $intern,
            ]);
        }
    //------------------------------------------------------------------------------------------------//


    //------------------------------------ Route pour supprimer une activité -------------------------//
        // Cette méthode est appelée lorsqu'un utilisateur souhaite supprimer une activité.
        #[Route('/Management/DeleteActivity/{id}', 'management.activity.delete', methods: ['GET','POST'])]
        #[IsGranted('ROLE_ADMIN')]
        public function deleteActivity(
            EntityManagerInterface $manager,
            Activity $activity,
        ): Response {

            // Vérifier si l'activité existe
            if(!$activity){
                return $this->redirectToRoute('management.index');
            }

            // Supprimer l'activité
            $manager->remove($activity);
            $manager->flush();

            // Rediriger vers la page de gestion des activités
            return $this->redirectToRoute('management.index');
        }
    //------------------------------------------------------------------------------------------------//
    
}