<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InternController extends AbstractController
{
    #[Route('/Intern', name:'intern.index', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('pages/intern/index.html.twig', [
            'controller_name' => 'InternController',
        ]);
    }
}
