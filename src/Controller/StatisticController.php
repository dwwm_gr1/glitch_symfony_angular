<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use App\Repository\InternRepository;
use App\Repository\RoomRepository;
use App\Service\StatisticService;
use App\Tools\Tools;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;



class StatisticController extends AbstractController
{
    #[Route('/Management/Statistic', 'statistic.index', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function index(
        Tools $tools,
        ChartBuilderInterface $chartBuilder,
        InternRepository $repositoryIntern,
        RoomRepository $repositoryRoom,
        StatisticService $statisticService
    ): Response
    {

    //---------------------------------------- Les Variable --------------------------------------//
        // Récupérer la liste des internes
        $listInterns = $repositoryIntern->findAll();
        // Compter le nombre total d'internes
        $totInterns = count($listInterns);
        // Initialiser le compteur des internes présents
        $internHere = 0;
        
        // Récupérer la liste des chambres
        $listRoom = $repositoryRoom->findAll();
        // Compter le nombre total de chambres
        $totRoom = count($listRoom);
        // Initialiser le compteur des chambres remplies
        $roomFill = 0;

        // Tableaux des jour de la semaine
        $dataWeekly = $statisticService->getStatsForCurrentWeek();
        // Tableaux de tout les mois
        $dataMonthly = $statisticService->getMonthlyTotals();

        // Liste de tout les statistique
        $statistics = $statisticService->getAllStat();


    //--------------------------------------------------------------------------------------------//

    //---------------------------------- Gestion des détail de statistic -------------------------//
        // Parcourir la liste des internes pour récupérer le nombre d'internes présents
        foreach ($listInterns as $intern) {
            if ($intern->getRoom()->getKeyState() == 0) {
                $internHere++;
            }
        }
        // Parcourir la liste des chambres pour récupérer le nombre de chambres remplies
        foreach ($listRoom as $room) {
            if ($room->getKeyState() == 0) {
                $roomFill++;
            }
        }

        // Calculer le taux de remplissage des chambres
        $dailyTax = ($roomFill / $totRoom * 100) . '%';
    //--------------------------------------------------------------------------------------------//

    //----------------------------------- Création des graphiques ----------------------------------//
        // Créer un graphique avec Taux journalier de remplissage
        $chartDaily = $tools->createChartPie(
            $chartBuilder,
            ['Présent', 'Absent'],
            'Taux journalier',
            ['#008C40', '#7e7f80'],
            [$internHere,($totInterns - $internHere)],
        );

        // Créer un graphique avec Taux Hebdomadaire de remplissage
        $chartWeekly = $tools->createChart(
            $chartBuilder,
            ['Lundi', 'Mardi', 'Mercredi', 'Jeudi'],
            'Taux Hebdomadaire',
            '#008C40',
            $dataWeekly,
            0,
            $totInterns
        );

        // Créer un graphique avec Taux mensuel de remplissage
        $chartMonthly = $tools->createChart(
            $chartBuilder,
            ['Janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'],
            'Taux Annuelle',
            '#008C40',
            $dataMonthly,
            0,
            500
        );

    //--------------------------------------------------------------------------------------------//

        return $this->render('pages/statistic/index.html.twig', [
            'chartDaily' => $chartDaily,
            'chartWeekly' => $chartWeekly,
            'chartMonthly' => $chartMonthly,
            'dailyTax' => $dailyTax,
            'totRoom' => $totRoom,
            'roomFill' => $roomFill,
            'internHere' => $internHere,
            'totInterns' => $totInterns,
            'statistics' => $statistics,
        ]);
    }

}
