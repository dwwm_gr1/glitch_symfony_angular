<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ObservationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class ObservationManagementController extends AbstractController
{
    #[Route('/Management/Observation', 'management.observation', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function index(ObservationRepository $obsRepository): Response
    {

        $observations = $obsRepository->findAll();
        // dd($observations);

        return $this->render('pages/observation_management/index.html.twig', [
            'observations' => $observations,
        ]);
    }

    #[Route('/Management/Observation/UpdateStatus/{id}/{status}', 'observation.update', methods: ['GET','POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function UpdateStatus(
        EntityManagerInterface $manager,
        ObservationRepository $observationRepository,
        int $id,
        string $status,
        ): Response
    {

        // Rechercher l'observation correspondante dans la base de données
        $observation = $observationRepository->find($id);

        if (!$observation) {
            throw $this->createNotFoundException('Observation not found');
        }

        // Mettre à jour le statut de l'observation
        $observation->setObservationState($status);

        // Enregistrer les modifications dans la base de données
        $manager->flush();

        // Rediriger vers la liste des observations
        return $this->redirectToRoute('management.observation');
    }
}
