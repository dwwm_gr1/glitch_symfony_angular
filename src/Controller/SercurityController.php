<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SercurityController extends AbstractController
{
    #[Route('/', name: 'sercurity.login', methods: ['GET', 'POST'])]
    public function login(): Response
    {
        return $this->render('pages/sercurity/login.html.twig');
    }

    #[Route('/logout', name: 'sercurity.logout', methods: ['GET', 'POST'])]
    public function logout()
    {
        # Nothing to do here...
    }
}
