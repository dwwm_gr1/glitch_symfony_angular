import {Modal, Popover} from 'bootstrap';
const $ = require('jquery');
global.$ = global.jQuery = $;
import '../bootstrap';


import Swal from 'sweetalert2';
window.Swal = Swal;

import 'datatables.net-dt';
import 'datatables.net-bs5';

// la dataTab des interne dans Manager
$("#dataTabIntern").DataTable({

    "responsive": true,
    "language": {
        "emptyTable":           "Aucun interne trouvé",
        "info":                 "",
        "infoEmpty":            "Aucun donnée disponible",
        "loadingRecords":       "Chargement des interne...",
        "processing":           "En cours...",
        "lengthMenu":           "Afficher _MENU_ entrées",
        "search":               "Rechercher un interne : ",
        "searchPlaceholder":    "Entrez votre recherche",
        "zeroRecords":          "L'interne recherché n'existe pas",
        "paginate": {
            "first":            "Premier",
            "last":             "Dernier",
            "next":             "Suivant",
            "previous":         "Précédent",
        }
    }
});

// la dataTab des activités dans Manager
const activityDt = $("#dataTabActivity").DataTable({

    "responsive": true,
    "columnDefs": [
        {
            orderable: false,
            targets: 4
        }
    ],
    "language": {
        "emptyTAble":           "Aucun activités trouvé",
        "info":                 "",
        "infoEmpty":            "Aucun donnée disponible",
        "loadingRecords":       "Chargement des activités...",
        "processing":           "En cours...",
        "lengthMenu":           "Afficher _MENU_ entrées",
        "search":               "Rechercher une activités : ",
        "searchPlaceholder":    "Entrez votre recherche",
        "zeroRecords":          "L'activités recherché n'existe pas",
        "paginate": {
            "first":            "Premier",
            "last":             "Dernier",
            "next":             "Suivant",
            "previous":         "Précédent",
        }
    }
});
// Les variable global

// Intern
const buttonCardIntern = document.querySelectorAll('.linkCard');

// Activité
const formAddActivity = document.querySelector('#form_add_activity');
const formUpdateActivity = document.querySelector('#form_update_activity');
const buttonAddActivity = document.querySelector('.addActivity');




// Les fonction

//------------------------------ Ouvrir les modal carte d'interne ---------------------------//

for (let intern = 0; intern < buttonCardIntern.length; intern++) {
    const element = buttonCardIntern[intern];
    element.addEventListener('click', function(event){
        getModalCardIntern(event);
    })
}

function getModalCardIntern(event) {
    event.preventDefault();
    let attribute = document.querySelector('.attribute');

    let idModal = attribute.getAttribute("data-idModal");
    let idModalBody = attribute.getAttribute("data-idModalBody");
    let requestUrl = attribute.getAttribute("data-requestUrl");


    let id = event.target.getAttribute("data-id");

    $.ajax({
        url: requestUrl + id,
        method: "GET",
        dataType: "html",
    })
    .done(function (response) {
        $("#" + idModalBody).html(response);
        $("#" + idModal).modal('show');
    })

    .fail(function (error) {
        console.log(error);
        window.Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Une erreur est survenue!',
        })
    })
}


//--------------------------------------------------------------------------------------------//
buttonAddActivity.addEventListener('click', function(e){
    e.preventDefault();
    $("#add-activity-modal").modal('show');
})
//------------------------------------ Ajoute d'une activité ---------------------------------//

    formAddActivity.addEventListener('submit', function(e){
        e.preventDefault();
        let formData = new FormData(e.target);

        fetch(this.action, {
            body: formData,
            method: 'POST'
        })
        .then(response => response.json())
        .then(json => {
            handleActivityResponse(json);
        });
    })

    const handleActivityResponse = function(response){

        removeErrors();
        switch (response.code){
            case 'ACTIVITY_ADDED_SUCCESSFULLY':
                $("#add-activity-modal").modal('hide');
                Swal.fire({
                    icon: 'success',
                    title: 'Ok !',
                    text: 'L\'activité a été ajouter.',
                })
                activityDt.row.add($(response.html));
                activityDt.rows().invalidate().draw(false);
                formAddActivity.reset();
                break;

            case 'ACTIVITY_INVALID_FORM':
                handleErrors(response.errors);
                break;
            
            default:
                $("#add-activity-modal").modal('hide');
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Une erreur est survenue!',
                })
                break;
        }
    }

    const handleErrors = function(errors) {
        if(errors.length === 0) return;
        
        for(const key in errors){
            let element = document.querySelector(`#add_activity_${key}`);
            element.classList.add('is-invalid');

            let div = document.createElement('div');
            div.classList.add('invalid-feedback', 'd-block');
            div.innerText = errors[key];

            element.after(div);
        }
    }

    const removeErrors = function() {
        const invalidFeedBackElements = document.querySelectorAll('.invalid-feedback');
        const isInvalidElements = document.querySelectorAll('.is-invalid');

        invalidFeedBackElements.forEach(invalidFeedBack => invalidFeedBack.remove());
        isInvalidElements.forEach(isInvalid => isInvalid.classList.remove('is-invalid'));

    }
//--------------------------------------------------------------------------------------------//


//------------------------------------ Supprimer une activité ---------------------------------//

function deleteElement(event) {
    event.preventDefault();
    let id = event.target.getAttribute("data-activityId");
    let requestUrl = "/Management/DeleteActivity/";
    let parent = $('#activity' + id);

    Swal.fire({
        title: 'Êtes-vous sûr ?',
        text: "Vous ne pourrez pas revenir en arrière !",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Annuler',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmer'
    })
    .then((result) => {
        if (result.isConfirmed) {
            Swal.fire(
                'Supprimé !',
                'L\'activité a été supprimé.',
                'success'
            )
            $.ajax({
                url: requestUrl + id,
                method: "GET",
                dataType: "html",
            })
            .done( (response) => {
                activityDt.row(parent).remove();
                activityDt.rows().invalidate().draw(false);
            })

            .fail((error) => {
                console.log(error);
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Une erreur est survenue!',
                })
            })
        }
    })
}
window.deleteActivity = deleteElement;
//--------------------------------------------------------------------------------------------//


//------------------------------------ Modifier une activité ---------------------------------//

function getActivity(event) {

    event.preventDefault();
    let id = event.target.getAttribute("data-id");

    $.ajax({
        url: "/Management/activity/get",
        type: "GET",
        data: { id: id },
        success: function (data) {

            // Remplir le formulaire de modification avec les données récupérées
            $('#update_activity_id').val(data.id);
            $('#update_activity_type').val(data.type);
            $('#update_activity_place').val(data.place);

            // Ouvrir la modal de modification
            $('#update-activity-modal').modal('show');
        },
        error: function (error) {
            console.log(error);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Une erreur est survenue!',
            })
        }
    });
}
window.getActivity = getActivity;

formUpdateActivity.addEventListener('submit', function(e){
    e.preventDefault();

    Swal.fire({
        title: 'Confirmation',
        text: "Voulez-vous modifier cette activité ?",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Annuler',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmer'
    })
    .then((result) => {
        if (result.isConfirmed) {
            fetch(this.action, {
                body: new FormData(e.target),
                method: 'POST'
            })
            .then(response => response.json())
            .then(json => {
                handleActivityUpdateResponse(json)
            });
        }
    })
})

const handleActivityUpdateResponse = function(response){

    removeUpdateErrors();
    switch (response.code){
        case 'ACTIVITY_UPDATE_SUCCESSFULLY':
            let row = document.querySelector('#activity' + response.id);
            let tds = row.querySelectorAll('td');
            tds[1].innerText = response.type;
            tds[3].innerText = response.place;

            $("#update-activity-modal").modal('hide');
            Swal.fire({
                icon: 'success',
                title: 'Ok !',
                text: 'L\'activité a été modifiée.',
            })
            activityDt.rows().invalidate().draw(false);
            break;

        case 'ACTIVITY_INVALID_UPDATE_FORM':
            handleUpdateErrors(response.errors);
            break;
        
        default:
            $("#update-activity-modal").modal('hide');
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Une erreur est survenue!',
            })
            break;
    }
}

const handleUpdateErrors = function(errors) {
    if(errors.length === 0) return;

    for(const key in errors){
        let element = document.querySelector(`#update_activity_${key}`);
        element.classList.add('is-invalid');

        let div = document.createElement('div');
        div.classList.add('invalid-feedback', 'd-block');
        div.innerText = errors[key];

        element.after(div);
    }
}

const removeUpdateErrors = function() {
    const invalidFeedBackElements = document.querySelectorAll('.invalid-feedback');
    const isInvalidElements = document.querySelectorAll('.is-invalid');

    invalidFeedBackElements.forEach(invalidFeedBack => invalidFeedBack.remove());
    isInvalidElements.forEach(isInvalid => isInvalid.classList.remove('is-invalid'));

}

//--------------------------------------------------------------------------------------------//
