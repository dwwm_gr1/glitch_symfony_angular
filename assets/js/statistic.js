import {Modal, Popover} from 'bootstrap';
const $ = require('jquery');
import '../bootstrap';
import 'datatables.net-dt';
import 'datatables.net-bs5';


// Les variable global
global.$ = global.jQuery = $;



// Les fonction

// la dataTab des interne dans Manager
$("#dataTabStat").DataTable({

    "responsive": true,
    "language": {
        "emptyTable":           "Aucun statistique trouvé",
        "info":                 "",
        "infoEmpty":            "Aucun donnée disponible",
        "loadingRecords":       "Chargement des statistiques...",
        "processing":           "En cours...",
        "lengthMenu":           "Afficher _MENU_ entrées",
        "search":               "Rechercher une statistique : ",
        "searchPlaceholder":    "Entrez votre recherche",
        "zeroRecords":          "La statistique recherché n'existe pas",
        "paginate": {
            "first":            "Premier",
            "last":             "Dernier",
            "next":             "Suivant",
            "previous":         "Précédent",
        }
    }
});

//--------------------------------------------------------------------------------------------//
