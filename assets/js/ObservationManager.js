import {Modal, Popover} from 'bootstrap';
const $ = require('jquery');
import '../bootstrap';
import 'datatables.net-dt';
import 'datatables.net-bs5';


// Les variable global
global.$ = global.jQuery = $;



// Les fonction

// la dataTab des interne dans Manager
$("#dataTabObs").DataTable({

    "responsive": true,
    "language": {
        "emptyTable":           "Aucun signalement trouvé",
        "info":                 "",
        "infoEmpty":            "Aucun donnée disponible",
        "loadingRecords":       "Chargement des signalements...",
        "processing":           "En cours...",
        "lengthMenu":           "Afficher _MENU_ entrées",
        "search":               "Rechercher un signalement : ",
        "searchPlaceholder":    "Entrez votre recherche",
        "zeroRecords":          "Le signalement recherché n'existe pas",
        "paginate": {
            "first":            "Premier",
            "last":             "Dernier",
            "next":             "Suivant",
            "previous":         "Précédent",
        }
    }
});

//--------------------------------------------------------------------------------------------//
