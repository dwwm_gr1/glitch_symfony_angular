# Glitch_Symfony_Angular

## Les prérequis pour que tout fonctionne correctement.


Dans le terminal CMD ou Powershell ouvert en administrateur, vérifiez votre version de PHP avec cette commande :  
```
php -v

```
Cela affichera la version de votre PHP installé (minimum -> PHP 8.x.x).  
Sinon, installez-le.  

---------------------------------------------------

Ensuite, vérifiez Composer avec cette commande :  
```
composer -v

```
Cela affichera la version de votre Composer installé (minimum -> Composer version 2.x.x).  
Sinon, installez-le.  

---------------------------------------------------

Ensuite, installez Scoop sur votre PC avec cette commande (si déjà installé, passez à la suite) :  

Exécutez cette commande :  
```
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
```

Puis celle-ci :  
```
irm get.scoop.sh | iex
```
Dans le cas d'une erreur, fermez le terminal et ouvrez-le normalement __sans mode administrateur__.   
Ensuite, retapez les commandes pour installer Scoop.  

---------------------------------------------------

Pour finir, installez la CLI de Symfony :  
```
scoop install symfony-cli
```

Passez à la suite ^_^ !  

---------------------------------------------------

## Getting started

Pour vous permettre de démarrer facilement, voici une liste des prochaines étapes recommandées :  

* Cloner le dépôt sur votre ordinateur où vous le souhaitez. Vous n'avez pas besoin de le placer dans le dossier Xampp.  

* URL du dépôt : https://gitlab.com/dwwm_gr1/glitch_symfony_angular.git  

* Ouvrez GitDesktop, cliquez sur la flèche située dans le bloc "Current repository" sur la gauche.  

* Cliquez sur le bouton "Add", puis "Clone repository".  

* Dans la fenêtre, cliquez sur "URL", ajoutez l'URL du dépôt et choisissez l'emplacement local où il sera cloné.  

* __Attention__ : vous ne pouvez pas travailler sur la branche "main" !  

* Cliquez sur "Current branch", puis "New branch". Entrez le nom de votre branche et validez. Puis publiez-la.  

Voilà, le projet est prêt. Ouvrez-le avec votre IDE (VSCode ou autre) et continuez la mise en place dans l'IDE.  

---------------------------------------------------

## Installation des component composer
 
Ouvrez un terminal et entrez la commande.  

```
composer install

```

---------------------------------------------------

## Créer la base de donnée

Puis les commande suivant.  

```
php bin/console doctrine:database:create

php bin/console doctrine:migrations:migrate

php bin/console doctrine:fixtures:load

```

---------------------------------------------------

## Initialisation des modules npm

```
npm install

```

---------------------------------------------------

## Test Unitaire

Prochainement les commandes à exécuter pour effectuer les tests unitaires.  

---------------------------------------------------

## Serveur de test pour le site

__Redémarrer votre pc avant de continuer__.

Commandes à exécuter pour lancer le serveur  

```
symfony server:start

```

---------------------------------------------------

## DataBase Local

Si votre PhpMyAdmin possède un nom d'utilisateur, un mot de passe et un port autre que par défaut, suivez ces étapes :  

Copiez le fichier .env qui se situe à la racine du projet, renommez-le en .env.local.  

Ouvrez-le et allez à la ligne :  
```
DATABASE_URL="mysql://root:@127.0.0.1:3306/Glitch_Symfo?serverVersion=mariadb-10.4.24&charset=utf8mb4"
```

---------------------------------------------------

Modifiez "root" par le nom de votre serveur.   
Si vous avez un mot de passe, ajoutez-le après les ":".   
Modifiez également le port.  

Exemple:  
```
DATABASE_URL="mysql://Mon_Serveur:Mon_Mdp@127.0.0.1:Mon_Port/Glitch_Symfo?serverVersion=mariadb-10.4.24&charset=utf8mb4"
```

Enregistrez les modifications et passez à la suite.

## Commande Php

Création d'un contrôler avec sa page associer:
```
php bin/console make:controller MaPage
```
__Utiliser du PacalCase__

Après la création de la page Déplacer le Template dans le dossier pages

Puis ouvrir le contrôler associer a la page et modifier le chemin et sécuriser la méthode

Exemple:
```
class MaPageController extends AbstractController
{
    #[Route('/MaPage', 'MaPage.index', methods:['GET'])]
    public function index(): Response
    {
        return $this->render('pages/MaPage/index.html.twig', [
            'controller_name' => 'MaPageController',
        ]);
    }
}
```

## Librairie utiliser est autre

Commandes Nodejs:
npm bootstrap  
npm install datatables.net-dt
npm install datatables.net-bs5
npm install sass-loader@^13.0.0 sass --save-dev 
npm install sweetalert2  
npm install --force && npm run build  
npm run dev  

Active la lecture continue des modification:  
npm run watch  

Commandes Symfony:
symfony new glitch_symfony_angular --version="6.2.*" --webapp 

Commandes Composer:
composer require symfony/orm-pack 
composer require --dev symfony/maker-bundle 
composer require --dev orm-fixtures
composer require fakerphp/faker --dev
composer require components/font-awesome
composer require symfony/webpack-encore-bundle
composer require symfony/ux-chartjs
composer require symfony/intl

lib:  
jQuery utiliser  
Sass/SCSS support Activer  
font-awesome  

## Le compte de test pour l'interne
login : testinterne@exemple.fr
password : interne

## Le compte de test pour l'employée
login : testmanager@exemple.fr
password : manager