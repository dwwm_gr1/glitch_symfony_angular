<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230404092418 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE activity (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(45) NOT NULL, positioning INT NOT NULL, place VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE emergency (id INT AUTO_INCREMENT NOT NULL, last_name VARCHAR(45) DEFAULT NULL, first_name VARCHAR(45) DEFAULT NULL, phone_number VARCHAR(25) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee (id INT AUTO_INCREMENT NOT NULL, last_name VARCHAR(45) NOT NULL, first_name VARCHAR(45) NOT NULL, email VARCHAR(45) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE intern (id INT AUTO_INCREMENT NOT NULL, room_id INT NOT NULL, emergency_id INT NOT NULL, wifi_id INT NOT NULL, last_name VARCHAR(30) NOT NULL, first_name VARCHAR(30) NOT NULL, serial_num_intern VARCHAR(45) NOT NULL, birth_date DATE NOT NULL, address_intern VARCHAR(50) NOT NULL, city VARCHAR(45) NOT NULL, zip_code INT NOT NULL, sex_intern VARCHAR(1) NOT NULL, phone_number_intern VARCHAR(25) NOT NULL, mail_intern VARCHAR(40) NOT NULL, number_plate VARCHAR(15) DEFAULT NULL, formation VARCHAR(100) NOT NULL, formation_abbreviation VARCHAR(10) NOT NULL, arriving_intern DATE NOT NULL, finishing_intern DATE NOT NULL, to_night INT NOT NULL, remaining_night INT NOT NULL, image_profil VARCHAR(100) DEFAULT NULL, UNIQUE INDEX UNIQ_A5795F3654177093 (room_id), UNIQUE INDEX UNIQ_A5795F36D904784C (emergency_id), UNIQUE INDEX UNIQ_A5795F365204490F (wifi_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inventory (id INT AUTO_INCREMENT NOT NULL, room_state INT NOT NULL, room_comment VARCHAR(100) DEFAULT NULL, performed DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE observation (id INT AUTO_INCREMENT NOT NULL, intern_id INT NOT NULL, type VARCHAR(25) NOT NULL, start_date DATE DEFAULT NULL, end_date DATE DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, observation_state VARCHAR(20) NOT NULL, images LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', INDEX IDX_C576DBE0525DD4B4 (intern_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE room (id INT AUTO_INCREMENT NOT NULL, inventory_id INT NOT NULL, room_number VARCHAR(5) NOT NULL, key_state INT NOT NULL, UNIQUE INDEX UNIQ_729F519B9EEA759 (inventory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE statistic (id INT AUTO_INCREMENT NOT NULL, daily_tax INT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscribe (id INT AUTO_INCREMENT NOT NULL, intern_id INT NOT NULL, activity_id INT NOT NULL, INDEX IDX_68B95F3E525DD4B4 (intern_id), INDEX IDX_68B95F3E81C06096 (activity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, intern_id INT DEFAULT NULL, employee_id INT DEFAULT NULL, full_name VARCHAR(50) NOT NULL, login VARCHAR(45) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_8D93D649525DD4B4 (intern_id), UNIQUE INDEX UNIQ_8D93D6498C03F15C (employee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wifi (id INT AUTO_INCREMENT NOT NULL, wifi_password VARCHAR(25) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE intern ADD CONSTRAINT FK_A5795F3654177093 FOREIGN KEY (room_id) REFERENCES room (id)');
        $this->addSql('ALTER TABLE intern ADD CONSTRAINT FK_A5795F36D904784C FOREIGN KEY (emergency_id) REFERENCES emergency (id)');
        $this->addSql('ALTER TABLE intern ADD CONSTRAINT FK_A5795F365204490F FOREIGN KEY (wifi_id) REFERENCES wifi (id)');
        $this->addSql('ALTER TABLE observation ADD CONSTRAINT FK_C576DBE0525DD4B4 FOREIGN KEY (intern_id) REFERENCES intern (id)');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519B9EEA759 FOREIGN KEY (inventory_id) REFERENCES inventory (id)');
        $this->addSql('ALTER TABLE subscribe ADD CONSTRAINT FK_68B95F3E525DD4B4 FOREIGN KEY (intern_id) REFERENCES intern (id)');
        $this->addSql('ALTER TABLE subscribe ADD CONSTRAINT FK_68B95F3E81C06096 FOREIGN KEY (activity_id) REFERENCES activity (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649525DD4B4 FOREIGN KEY (intern_id) REFERENCES intern (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6498C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE intern DROP FOREIGN KEY FK_A5795F3654177093');
        $this->addSql('ALTER TABLE intern DROP FOREIGN KEY FK_A5795F36D904784C');
        $this->addSql('ALTER TABLE intern DROP FOREIGN KEY FK_A5795F365204490F');
        $this->addSql('ALTER TABLE observation DROP FOREIGN KEY FK_C576DBE0525DD4B4');
        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519B9EEA759');
        $this->addSql('ALTER TABLE subscribe DROP FOREIGN KEY FK_68B95F3E525DD4B4');
        $this->addSql('ALTER TABLE subscribe DROP FOREIGN KEY FK_68B95F3E81C06096');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649525DD4B4');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6498C03F15C');
        $this->addSql('DROP TABLE activity');
        $this->addSql('DROP TABLE emergency');
        $this->addSql('DROP TABLE employee');
        $this->addSql('DROP TABLE intern');
        $this->addSql('DROP TABLE inventory');
        $this->addSql('DROP TABLE observation');
        $this->addSql('DROP TABLE room');
        $this->addSql('DROP TABLE statistic');
        $this->addSql('DROP TABLE subscribe');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE wifi');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
